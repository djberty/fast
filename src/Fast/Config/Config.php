<?php

namespace Fast\Config;

use Fast\App;

class Config implements \Iterator, \ArrayAccess
{
    const CONFIG_ROOT = 'Config/';

    /**
     * @var array container for key value pairs
     */
    protected $_config = [];

    /**
     * @var string
     */
    protected static $env = null;

    /**
     * Gets the current environment
     * @return string
     */
    public static function getEnvironment()
    {
        if (static::$env === null) {
            if (isset($_SERVER['ENVIRONMENT'])) {
                static::$env = $_SERVER['ENVIRONMENT'];
            } else {
                static::$env = 'live';
            }
        }
        return static::$env;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->_config[$name] = $value;
    }

    /**
     * @param string $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name)
    {
        if (isset($this->_config[$name])) {
            return $this->_config[$name];
        }
        throw new Exception("Unknown config name: " . $name);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->_config[$name]);
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return current($this->_config);
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        next($this->_config);
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return key($this->_config);
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->_config[key($this->_config)]);
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        reset($this->_config);
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset
     * @return boolean true on success or false on failure.
     */
    public function offsetExists($offset)
    {
        return isset($this->_config[$offset]);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        return $this->_config[$offset];
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset
     * @param mixed $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->_config[$offset] = $value;
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->_config[$offset]);
    }


    /**
     * @param array $config
     * @return Config
     */
    public static function fromArray(array $config)
    {
        $cfg = new static();
        foreach ($config as $name => $value) {
            if (is_array($value)) {
                $cfg->$name = static::fromArray($value);
            } else {
                $cfg->$name = $value;
            }
        }
        return $cfg;
    }

    /**
     * @param string $json
     * @throws Exception
     * @return Config
     */
    public static function fromJSON($json)
    {
        $config = json_decode($json, true);
        if ($config) {
            return static::fromArray($config);
        } else {
            throw new Exception("Invalid JSON");
        }
    }

    /**
     * @param string $json_file
     * @param null | string auth file for specific environment
     * @throws Exception
     * @return Config
     */
    public static function fromJSONFile($json_file, $within_env=null)
    {
        if ($within_env === null) {
            $within_env = static::getEnvironment();
        }
        if (static::configJSONExists($json_file)) {
            $json = file_get_contents(App::getAppPath() . static::CONFIG_ROOT . $within_env . '/' . $json_file);
        } elseif ($within_env != 'live' && static::configJSONExists($json_file, 'live')) {
            $json = file_get_contents(App::getAppPath() . static::CONFIG_ROOT . 'live/' . $json_file);
        } else {
            throw new Exception("Config JSON file not found: " . $json_file);
        }
        if ($json) {
            $config = static::fromJSON($json);
            // check for and deal with configs extending other configs
            if (isset($config['extends'])) {
                $secondary_config = static::fromJSONFile($config["extends"]->name, $config["extends"]->env);
                $config = static::mergeConfigs($secondary_config, $config);
                unset($config['extends']);
            }
            return $config;
        } else {
            throw new Exception("Could not read Config JSON file: " . $json_file);
        }
    }

    /**
     * @param string $config_name With or without the .json file extension
     * @param string $within_env the config environment folder to check in
     * @return bool
     */
    public static function configJSONExists($config_name, $within_env=null)
    {
        if ($within_env === null) {
            $within_env = static::getEnvironment();
        }
        return is_file(App::getAppPath() . static::CONFIG_ROOT . $within_env . '/' . $config_name);
    }

    /**
     * @param Config $config_primary
     * @param Config ...
     * @return Config
     */
    public static function mergeConfigs(Config $config_primary)
    {
        /** @var Config[] $configs */
        $configs = func_get_args();
        $base = array_shift($configs);

        foreach ($configs as $append) {
            foreach ($append as $key => $value) {
                if (!isset($base->$key)) {
                    $base->$key = $append[$key];
                    continue;
                }
                if ($value instanceof Config || $base->$key instanceof Config) {
                    $base->$key = self::mergeConfigs($base->$key, $append[$key]);
                } else {
                    $base->$key = $value;
                }
            }
        }
        return $base;
    }
} 