<?php

namespace Fast\Controller;

use Fast\App;
use Fast\Auth\Auth;
use Fast\Autoloader\Autoloader;
use Fast\Config\Config;
use Fast\Model\Model;
use Fast\Model\MySQL;
use Fast\Request\Session;
use Fast\View\View;

class Controller
{
    /**
     * @var App
     */
    protected $app = null;

    /**
     * @var Auth
     */
    protected $auth = null;

    /**
     * @var View[]
     */
    protected $views = [];

    /**
     * Valid HTTP methods for this controller
     * @var string[]
     */
    protected $valid_methods = [
        'get'
    ];

    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * Should be overridden in controllers to establish that the controller requires authentication
     * @return bool
     */
    public function requiresAuthentication()
    {
        return false;
    }

    /**
     * @return Auth
     */
    public function getAuth()
    {
        if ($this->auth === null) {
            $session = new Session('Auth');
            $config = null;
            if (Config::configJSONExists('auth.json')) {
                $config = Config::fromJSONFile('auth.json');
            }
            $this->auth = new Auth($session, $config);
        }
        return $this->auth;
    }

    /**
     * @return App
     */
    protected function getApp()
    {
        return $this->app;
    }

    /**
     * @param string $name (Optional) If omitted will return the view named according to the controller.
     * @return View
     */
    protected function getView($name=null)
    {
        if (!$name) {
            $name = str_replace(['App\\', 'Controller'], '', get_class($this));
            $name = lcfirst(trim($name, '\\'));
        }
        if (!isset($this->views[$name])) {
            $this->views[$name] = new View($name);
            $this->views[$name]->setApp($this->getApp());
        }
        return $this->views[$name];
    }

    /**
     * @param View $view
     * @return static
     */
    protected function setView(View $view)
    {
        $this->view = $view;
        return $this;
    }

    /**
     * Loads and returns an instance of the named model
     * @param string $name
     * @param string[] An array of data to populate the model with (Optional)
     * @return Model
     * @throws Exception
     */
    protected function getModel($name, $data = [])
    {
        if (Autoloader::instance()->loadModel($name)) {
            $classname = '\\App\\Models\\' . $name;
            $model = new $classname();
            if ($model instanceof MySQL) {
                $model->setDB($this->getApp()->getDB());
            }
            return $model;
        }
        throw new Exception("Could not load model (probably does not exist): " . $name);
    }

    /**
     * @return string[]
     */
    public function getValidMethods()
    {
        return $this->valid_methods;
    }

    /**
     * Sets-up and sends the response for an HTTP redirect
     * @param string $destination_url
     * @param int $status_code (default: 301)
     */
    protected function redirect($destination_url, $status_code=301)
    {
        $request = $this->getApp()->getRequest();
        $response = $this->getApp()->getResponse();
        if (strpos($destination_url, 'http') === false) {
            $destination_url = $request->getProtocol() . '://' . $request->getHost() . '/' . ltrim($destination_url, '/');
        }
        $response->setStatus($status_code)
                 ->setBody('')
                 ->setHeader('Location', $destination_url)
                 ->render();
        die;
    }

    /**
     * Gets a URL based on a named route
     * @param string $name
     * @param string[] $params
     * @return string
     * @throws Exception
     */
    protected function url($name, $params = [])
    {
        $route = $this->getApp()->getRouter()->getRouteByName($name);
        if (!$route) {
            throw new Exception("Cannot find route for generating a URL: " . $name);
        }
        return $route->generateURL($params);
    }

    // Hooks for User-land controllers
    public function preAction() {}
    public function postAction() {}
    public function preOutput(&$output) {}
    public function postOutput(&$output) {}
} 