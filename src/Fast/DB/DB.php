<?php

namespace Fast\DB;

use Fast\Config\Config;

class DB extends \PDO
{
    public function __construct(Config $config=null)
    {
        $dsn = $config->dsn;
        $username = $config->username;
        $password = $config->password;

        parent::__construct ($dsn, $username, $password);

        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->setAttribute(\PDO::ATTR_STATEMENT_CLASS, ['\\Fast\\DB\\Statement']);
        $this->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);

        $this->prepare("SET NAMES 'utf8'")->execute();
    }
}