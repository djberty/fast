<?php

namespace Fast;

class Set implements \Countable, \Iterator, \ArrayAccess
{
    const DEFAULT_NAMESPACE = '__DEFAULT__';

    /**
     * The current namespace
     * @var string
     */
    protected $namespace = '';

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param array $data (Optional)
     * @param string | null $namespace (Optional) Will use const::DEFAULT_NAMESPACE if not set
     */
    public function __construct(array $data = [], $namespace=null)
    {
        if ($namespace === null) {
            $namespace = static::DEFAULT_NAMESPACE;
        }
        $this->namespace = $namespace;
        $this->data[$this->namespace] = $data;
    }

    /**
     * Gets the current namespace
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * Gets the named value in the current namespace
     * @param string $name
     * @return mixed | null
     */
    public function get($name)
    {
        if (isset($this->data[$this->getNamespace()][$name])) {
            return $this->data[$this->getNamespace()][$name];
        }
        return null;
    }

    /**
     * Sets the names value in the current namespace
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function set($name, $value)
    {
        $this->data[$this->getNamespace()][$name] = $value;
        return $this;
    }

    /**
     * Removes a names value
     * @param string $name
     * @return $this
     */
    public function remove($name)
    {
        unset($this->data[$this->getNamespace()][$name]);
        return $this;
    }

    /**
     * Clears a named value
     * @param string $name
     * @return $this
     */
    public function clear($name)
    {
        $this->data[$this->getNamespace()][$name] = null;
        return $this;
    }

    /**
     * Gets all the data in the current namespace
     * @return array
     */
    public function all()
    {
        return $this->data[$this->getNamespace()];
    }

    /**
     * Gets a count of values in the current namespace
     * @return int
     */
    public function count()
    {
        return count($this->data[$this->getNamespace()]);
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->get(key($this->data[$this->getNamespace()]));
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        next($this->data[$this->getNamespace()]);
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return key($this->data[$this->getNamespace()]);
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->data[$this->getNamespace()][key($this->data)]);
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        reset($this->data[$this->getNamespace()]);
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$this->getNamespace()][$offset]);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$this->getNamespace()][$offset]);
    }
} 