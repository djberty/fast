<?php

namespace Fast;

class Exception extends \Exception
{
    /**
     * The HTTP status code to use in the browser response
     * @var int (default: 500)
     */
    private $status_code = 500;

    /**
     * @param string $message [optional] The Exception message to throw.
     * @param int $code [optional] The Exception code.
     * @param \Exception $previous [optional] The previous exception used for the exception chaining
     */
    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        // if the code greater than 100, treat as an HTTP status code
        if ($code >= 100) {
            $this->setStatusCode($code);
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * @param $status_code
     */
    public function setStatusCode($status_code)
    {
        $this->status_code = $status_code;
    }
} 