<?php

namespace Fast\View;

use Fast\App;
use Fast\Config\Config;

class View
{
    /**
     * @var string
     */
    protected $_charset = 'UTF-8';

    /**
     * @var string
     */
    protected $_layout_name = null;

    /**
     * @var string The name of this view
     */
    protected $_name = '';

    /**
     * @var string[]
     */
    protected $_values = [];

    /**
     * @var Block[]
     */
    protected $_blocks = [];

    /**
     * @var App
     */
    protected $app = null;

    public function __construct($name, Config $config = null)
    {
        $this->_name = $name;
        if ($config) {
            if (isset($config->charset)) {
                $this->_charset = $config->charset;
            }
        }
    }

    /**
     * @param App $app
     * @return static;
     */
    public function setApp(App $app)
    {
        $this->app = $app;
        return $this;
    }

    /**
     * The view's name
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Returns the array of values set in this view
     * @return mixed[]
     */
    public function getValues()
    {
        return $this->_values;
    }

    /**
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function setValue($name, $value)
    {
        $this->_values[$name] = $value;
        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function setValues($values)
    {
        foreach ($values as $name => $value) {
            $this->setValue($name, $value);
        }
        return $this;
    }

    /**
     * Sets the layout surrounding this view
     * @param string $layout_name
     */
    public function setLayout($layout_name)
    {
        $this->_layout_name = $layout_name;
    }

    /**
     * A "block" is an arbitrary piece of view content which can be set in a view and then rendered
     * in a layout (or set and rendered in the same view)
     * @param string $name
     * @return Block
     */
    public function getBlock($name)
    {
        if (!isset($this->_blocks[$name])) {
            $this->_blocks[$name] = new Block($name);
        }
        return $this->_blocks[$name];
    }

    /**
     * Imports another view as a partial
     * @param string $name
     * @param string[] $values (optional)
     * @return string
     */
    public function partial($name, array $values = [])
    {
        $values = array_merge($values, $this->_values);
        return $this->import('Partials/' . $name, $values);
    }

    /**
     * Imports and renders another view, this method does not import the values from the parent view
     * @param string $name
     * @param string[] $values (optional)
     * @return string
     */
    public function import($name, array $values = [])
    {
        $view = new View($name);
        $view->setApp($this->app);
        $view->setValues($values);
        return $view->render();
    }

    /**
     * Generates a URL based on a named route
     * @param string $name
     * @param string[] $params
     * @throws Exception
     * @return string
     */
    public function url($name, $params = [])
    {
        $route = $this->app->getRouter()->getRouteByName($name);
        if (!$route) {
            throw new Exception("Could not find a route for generating a URL with the name: " . $name);
        }
        return $route->generateURL($params);
    }

    /**
     * @param string $name
     * @return mixed
     * @throws Exception
     */
    public function getValue($name)
    {
        if (isset($this->_values[$name])) {
            return $this->_values[$name];
        }
        throw new Exception("View value not set: " . $name);
    }

    /**
     * Used in view files to retrieve values from values array
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->getValue($name);
    }

    /**
     * @param string $value
     * @return string
     */
    public function escape($value)
    {
        return htmlspecialchars($value, ENT_QUOTES, $this->_charset);
    }

    /**
     * @param string $view_name (defaults to name of current view)
     * @throws Exception
     * @return string
     */
    public function render($view_name = null)
    {
        if (!$view_name) {
            $view_name = $this->_name;
        }
        try {

            if ($this->_layout_name !== null) {

                $view_path = App::getAppPath() . 'Views/' . $view_name . '.php';
                if (is_file($view_path)) {
                    ob_start();
                    include $view_path;
                    $this->_values['__view__'] = ob_get_contents();
                    ob_end_clean();
                } else {
                    throw new Exception("Could not locate view file: " . $view_name);
                }

                $layout_path = App::getAppPath()  . 'Views/Layouts/' . $this->_layout_name . '.php';

                if (is_file($layout_path)) {
                    ob_start();
                    include $layout_path;
                    $output = ob_get_contents();
                    ob_end_clean();
                } else {
                    throw new Exception("Could not locate layout file: " . $this->_layout_name);
                }

            } else {

                $view_path = App::getAppPath() . 'Views/' . $view_name . '.php';


                if (is_file($view_path)) {
                    ob_start();
                    include $view_path;
                    $output = ob_get_contents();
                    ob_end_clean();
                } else {
                    throw new Exception("Could not locate view file: " . $view_name);
                }
            }

            return $output;
        } catch (\Fast\Autoloader\Exception $e) {
            throw new Exception("Could not load View: " . $view_name);
        }
    }
}