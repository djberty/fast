<?php

namespace Fast\Model;

class MySQLDate
{
    private $timestamp = 0;

    public function __construct($timestamp = null)
    {
        if ($timestamp === null) {
            $timestamp = time();
        }
        $this->timestamp = $timestamp;
    }

    public function asTimestamp()
    {
        return $this->timestamp;
    }

    public function asMySQLDateTime()
    {
        return date('Y-m-d H:i:s', $this->timestamp);
    }

    public static function fromMySQLDate($date)
    {
        return new static(strtotime($date));
    }

    public static function fromUnixTimestamp($ts)
    {
        return new static($ts);
    }
} 