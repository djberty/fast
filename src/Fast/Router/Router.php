<?php

namespace Fast\Router;

use \Fast\Config\Config;
use Fast\Request\Request;

class Router {

    /**
     * @var Route[]
     */
    protected $routes = [];

    public function __construct(Config $config = null)
    {
        if ($config) {
            if (isset($config->routes)) {
                foreach ($config->routes as $name => $r) {
                    $route = new Route($name, $r->route, $r->controller);
                    if (!isset($r->type)) {
                        $r->type = Route::TYPE_MATCH;
                    }
                    $route->setType($r->type);
                    $this->addRoute($route);
                }
            }
        }
    }

    /**
     * @param Route $route
     * @return static
     */
    public function addRoute(Route $route) {
        $this->routes[] = $route;
        return $this;
    }

    /**
     * @return Route[]
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Gets a route by it's name
     * @param string $name
     * @return Route | null
     */
    public function getRouteByName($name)
    {
        foreach ($this->getRoutes() as $route) {
            if ($route->getName() == $name) {
                return $route;
            }
        }
        return null;
    }

    /**
     * @param string $uri
     * @param Request $request
     * @throws Exception
     * @return Route
     */
    public function match($uri, Request $request) {
        foreach ($this->routes as $route) {
            if ($route->isMatch($uri, $request)) {
                return $route;
            }
        }
        throw new Exception("Router failed to find route for URI: " . $uri);
    }
} 