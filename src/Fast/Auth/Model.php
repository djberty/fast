<?php

namespace Fast\Auth;

abstract class Model extends \Fast\Model\Model
{
    /**
     * @var Credentials
     */
    protected $credentials = null;

    /**
     * Whether the credentials have been loaded yet
     * @var bool
     */
    protected $credentials_loaded = false;

    /**
     * Sets the username which will be used to load password and salt
     * @param string $username
     * @return mixed
     */
    public function setUsername($username)
    {
        $this->credentials = new Credentials($username);
    }

    /**
     * Gets credentials after they have been passed through the concrete loadCredentials() method
     * @return Credentials
     * @throws Exception
     */
    public function getCredentials()
    {
        if ($this->credentials === null) {
            throw new Exception("setUsername() must be called before loaded credentials can be obtained");
        }
        if (!$this->credentials_loaded) {
            $this->credentials_loaded = $this->loadCredentials($this->credentials);
            if (!$this->credentials_loaded) {
                throw new Exception("Failed to load credentials for the username: " . $this->credentials->getUsername());
            }
        }
        return $this->credentials;
    }

    /**
     * Populates the passed in Credentials object, must return bool indicating whether credentials were found
     * @param Credentials $credentials
     * @return bool
     */
    abstract public function loadCredentials(Credentials $credentials);

    /**
     * @return bool|void
     */
    public function save()
    {
        return $this->saveCredentials($this->credentials);
    }

    /**
     * @param Credentials $credentials
     * @return bool
     */
    abstract public function saveCredentials(Credentials $credentials);

    // Methods to fulfill needs of Model
    protected function deleteRow() {}
    protected function getLastInsertId() {}
    protected function loadRow() {}
    protected function saveRow(array $save_data, $save_type) {}
}