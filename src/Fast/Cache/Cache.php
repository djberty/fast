<?php

namespace Fast\Cache;

abstract class Cache
{
    const DEFAULT_TTL = 0;

    /**
     * @param string $name
     * @return mixed
     */
    public function get($name)
    {
        return $this->_get($name);
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param int $ttl (Optional, defaults to self::DEFAULT_TTL)
     * @return mixed
     */
    public function set($name, $value, $ttl = self::DEFAULT_TTL)
    {
        return $this->_set($name, $value, $ttl);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function exists($name)
    {
        return $this->_exists($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function delete($name)
    {
        return $this->_delete($name);
    }

    /**
     * @param string $name
     * @return mixed
     */
    abstract protected function _get($name);

    /**
     * @param string $name
     * @param mixed $value
     * @param int $ttl 0 (zero) means infinite TTL
     * @return mixed
     */
    abstract protected function _set($name, $value, $ttl);

    /**
     * @param string $name
     * @return bool
     */
    abstract protected function _exists($name);

    /**
     * @param string $name
     * @return bool
     */
    abstract protected function _delete($name);
} 