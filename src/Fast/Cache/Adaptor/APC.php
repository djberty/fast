<?php

namespace Fast\Cache\Adaptor;

use Fast\Cache\Cache;
use Fast\Cache\Exception;

class APC extends Cache
{
    public function __construct()
    {
        if (!extension_loaded('apc')) {
            throw new Exception('PHP APC Extension is not installed');
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    protected function _get($name)
    {
        return apc_fetch($name);
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param int $ttl 0 (zero) means infinite TTL
     * @return mixed
     */
    protected function _set($name, $value, $ttl)
    {
        return apc_store($name, $value, $ttl);
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function _exists($name)
    {
        return apc_exists($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function _delete($name)
    {
        return apc_delete($name);
    }
} 