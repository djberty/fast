<?php

namespace Fast\Cache\Adaptor;

use Fast\Cache\Cache;

class File extends Cache
{
    protected $path = null;

    public function __construct($path = null)
    {
        if (!$path) {
            $path = '/tmp/';
        } else {
            $path = realpath(FAST_ROOT . ltrim($path, '/'));
        }
        $this->path = $path;
    }

    /**
     * @param string $name
     * @return mixed
     */
    protected function _get($name)
    {
        $path = $this->getPath($name);
        if (is_file($path)) {
            $value = unserialize(file_get_contents($path));
            if ($value[1] > time()) {
                $this->_delete($name);
                return null;
            }
            return $value[0];
        }
        return null;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param int $ttl 0 (zero) means infinite TTL
     * @return mixed
     */
    protected function _set($name, $value, $ttl)
    {
        if ($ttl > 0) {
            $ttl = time() + $ttl;
        }
        $path = $this->getPath($name);
        $value = [
            0 => $value,
            1 => (int) $ttl
        ];
        return file_put_contents($path, serialize($value));
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function _exists($name)
    {
        return $this->_get($name) !== null;
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function _delete($name)
    {
        $path = $this->getPath($name);
        if (is_file($path)) {
            return unlink($path);
        }
        return false;
    }

    /**
     * @param string $name
     * @return string
     */
    private function getToken($name)
    {
        return md5($name);
    }

    /**
     * @param string $name
     * @return string
     */
    private function getPath($name)
    {
        return $this->path . $this->getToken($name) . '.cache';
    }
} 