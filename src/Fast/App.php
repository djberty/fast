<?php

namespace Fast;

use Fast\Auth\Auth;
use Fast\Autoloader\Autoloader;
use Fast\Config\Config;
use Fast\DB\DB;
use Fast\Request\Request;
use Fast\Response\Response;
use Fast\Router\Router;
use Fast\View\View;

class App
{
    const DEFAULT_APP_PATH = '../app/';

    /**
     * @var null|string
     */
    protected static $app_path = null;

    /**
     * @var Auth
     */
    protected $auth = null;

    /**
     * @var Router
     */
    protected $router = null;

    /**
     * @var Config
     */
    protected $config = null;

    /**
     * @var Request
     */
    protected $request = null;

    /**
     * @var Response
     */
    protected $response = null;

    /**
     * @var DB
     */
    protected $db = null;

    /**
     * Whether debugging is enabled
     * @var bool
     */
    protected static $debug = false;

    /**
     * The current environment
     * @var string
     */
    protected $env = 'live';

    /**
     * The starting timestamp
     * @var int
     */
    protected $start_time = 0;

    /**
     * @param null|string $env
     */
    public function __construct($env=null)
    {
        if ($env === null) {
            $env = Config::getEnvironment();
        }
        if ($env == 'dev') {
            self::debug();
        }
        $this->env = $env;
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function getAppPath()
    {
        if (self::$app_path === null) {
            if (defined('APP_PATH') && is_dir(APP_PATH)) {
                self::$app_path = realpath(APP_PATH);
            } elseif (is_dir(self::DEFAULT_APP_PATH)) {
                self::$app_path = self::DEFAULT_APP_PATH;
            } else {
                throw new Exception('Unable to establish the application (app) code path.');
            }
            self::$app_path = rtrim(self::$app_path, '/') . '/';
        }
        return self::$app_path;
    }

    /**
     * @return string
     */
    public function getEnvironment()
    {
        return $this->env;
    }

    public function getAuth()
    {
        if ($this->auth === null) {
            $config = null;
            if (Config::configJSONExists('auth.json')) {
                $config = Config::fromJSONFile('auth.json');
            }
            $this->auth = new Auth($config);
        }
        return $this->auth;
    }

    /**
     * @param Router $router
     */
    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @return Router
     */
    public function getRouter()
    {
        if ($this->router === null) {
            $config = Config::fromJSONFile('router.json');
            $this->setRouter(new Router($config));
        }
        return $this->router;
    }

    /**
     * @param Response $response
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        if ($this->response === null) {
            $config = null;
            if (Config::configJSONExists('response.json')) {
                $config = Config::fromJSONFile('response.json');
            }
            $this->setResponse(new Response($config));
        }
        return $this->response;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        if ($this->request === null) {
            $config = null;
            if (Config::configJSONExists('request.json')) {
                $config = Config::fromJSONFile('request.json');
            }
            $this->setRequest(new Request($config));
        }
        return $this->request;
    }

    /**
     * Gets an instance of the DB object (wraps PDO)
     */
    public function getDB()
    {
        if ($this->db === null) {
            $config = Config::fromJSONFile('db.json');
            $this->db = new DB($config);
        }
        return $this->db;
    }

    /**
     * Runs the App
     */
    public function run()
    {
        if (self::$debug) {
            $this->start_time = microtime(true);
        }

        try {

            $route = $this->getRouter()->match($this->getRequest()->getPath(), $this->getRequest());
            $controller = $route->getController();
            $action = strtolower($this->getRequest()->getMethod());

            Autoloader::instance()->loadController($controller);

            $controller = '\\App\\Controller\\' . $controller;
            $ctrl = new $controller($this);
            if (!$ctrl instanceof Controller\Controller) {
                throw new Exception("Controller not a sub-class of \\Fast\\Controller\\Controller: " . $controller);
            }
            if ($ctrl->requiresAuthentication() && !$ctrl->getAuth()->isAuthenticated()) {
                throw new Exception('Unauthorised Access', 403);
            }

            if (in_array($action, $ctrl->getValidMethods())) {

                if (!method_exists($ctrl, $action)) {
                    throw new Exception(sprintf("Action: %s not found in controller: %s", $action, $controller));
                }

                $ctrl->preAction();
                $output = $ctrl->$action();
                $ctrl->postAction();

                $ctrl->preOutput($output);
                $this->getResponse()->setBody($output)->render();
                $ctrl->postOutput($output);

            } else {
                throw new Exception("HTTP method not allowed: " . $action, 404);
            }

        } catch (Exception $e) {

            $status_code = $e->getStatusCode();
            if ($status_code < 100) {
                $status_code = 500;
            }
            $this->getResponse()->setStatus($status_code);
            $this->displayError($e);

        } catch (\Exception $e) {
            // @todo convert exception to a \Fast\Exception and display
        }

//        if (static::$debug && !in_array('application/json', $this->getRequest()->getAcceptedContentTypes())) {
//            echo '<pre>Execution Time: ' . (round(microtime(true) - $this->start_time, 5) * 1000) . ' ms</pre>';
//        }
    }

    public function displayError(Exception $e)
    {
        $view = new View('Errors/default');
        $view->setApp($this);
        $view->setValue('exception', $e);
        $view->setLayout('error');

        $this->getResponse()->setBody($view->render())->render();
    }

    /**
     * Enables / Disables debug mode
     * @param bool $state (default true)
     */
    public static function debug($state = true)
    {
        static::$debug = $state;
    }
} 