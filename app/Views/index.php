<?php $this->getBlock('title')->setContent('Default Home Page') ?>

<?php $this->getBlock('js')->start() ?>
    <script type="text/javascript" src="/js/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="/js/underscore.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
<?php $this->getBlock('js')->end() ?>

<?php $this->getBlock('css')->start() ?>
    <link type="text/css" rel="stylesheet" href="/css/main.css" />
<?php $this->getBlock('css')->end() ?>

<?php $this->getBlock('meta')->start() ?>
    <!-- Meta Data -->
<?php $this->getBlock('meta')->end() ?>

<?php $this->getBlock('content')->start() ?>

    <header>
        <div id="links">
            <span class="not-authenticated">
                <a id="login-link" href="#login" class="modal">Login</a> |
                <a id="register-link" href="#register" class="modal">Register</a>
            </span>
            <span class="authenticated">
                <a id="logout-link" href="#logout">Logout</a>
            </span>
        </div>
        <h1>Default Home Page</h1>
    </header>

    <section>
        Content here!!!
    </section>

    <div id="modals">

        <div id="login" class="window">
            <a href="#" class="close">X</a>
            <div class="modal_content"></div>
        </div>

        <div id="register" class="window">
            <a href="#" class="close">X</a>
            <div class="modal_content"></div>
        </div>

        <div id="mask"></div>

    </div>
<?php $this->getBlock('content')->end() ?>