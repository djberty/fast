<!DOCTYPE html>
<html>
<head>
    <title><?php echo $this->escape($this->getBlock('title')->output()) ?></title>
    <?php echo $this->getBlock('meta')->output() ?>
    <?php echo $this->getBlock('css')->output() ?>
</head>
<body>
<?php echo $this->getBlock('content')->output() ?>
<?php echo $this->getBlock('js')->output() ?>
</body>
</html>