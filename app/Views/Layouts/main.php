<!DOCTYPE html>
<html>
<head>

    <title><?php echo $this->escape($this->getBlock('title')->output()) ?></title>

    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0">
    <?php echo $this->getBlock('meta')->output() ?>

    <?php echo $this->getBlock('css')->output() ?>
</head>
<body>
    <?php echo $this->getBlock('content')->output() ?>
    <?php echo $this->getBlock('js')->output() ?>
</body>
</html>