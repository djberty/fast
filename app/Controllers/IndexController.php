<?php

namespace App\Controller;

use Fast\Controller\Controller;

class IndexController extends Controller
{
    public function get()
    {
        $this->getView()->setLayout('main');
        return $this->getView()->render();
    }
}